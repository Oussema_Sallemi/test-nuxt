export default async function({ route, store, redirect, app }) {
  if (
    route.path === "/user-dashboard" &&
    app.$cookies.get("vuex").auth.loggedIn === false
  ) {
    redirect("/not-loggedIn");
  }
}
