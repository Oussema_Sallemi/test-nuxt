export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "test-all",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" }
    ],
    script: [
      { src: "https://apis.google.com/js/platform.js" },
      { src: "https://code.jquery.com/jquery-3.3.1.slim.min.js" },
      {
        src:
          "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      },
      {
        src:
          "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      }
    ],
    link: [
      {
        rel: "icon",
        type: "image/x-icon",
        href: "/favicon.ico"
      },
      {
        rel: "stylesheet",
        type: "text/css",
        href:
          "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    "@/assets/style/main.css",
    "@/assets/style/font-awesome.min.css",
    "@/assets/style/plugins.css"
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    //'@nuxtjs/eslint-module'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    //"bootstrap-vue/nuxt",
    // https://go.nuxtjs.dev/axios
    "@nuxtjs/axios",
    // https://go.nuxtjs.dev/pwa
    "@nuxtjs/pwa",
    // https://go.nuxtjs.dev/content
    "@nuxt/content",
    "nuxt-i18n",
    "@nuxtjs/dotenv",
    "@nuxtjs/auth-next",
    // Simple usage
    "cookie-universal-nuxt"
  ],
  i18n: {
    locales: [
      { code: "de-de", iso: "de-DE", file: "de-de.json", name: "Deutchland" },
      { code: "en", iso: "en", file: "en.json", name: "United Kingdom" },
      { code: "es-es", iso: "es-ES", file: "es-es.json", name: "España" },
      { code: "fr", iso: "fr", file: "fr.json", name: "France" },
      { code: "it-it", iso: "it-IT", file: "it-it.json", name: "Italia" },
      { code: "nl-nl", iso: "nl-NL", file: "nl-nl.json", name: "Nederlands" },
      { code: "pt-pt", iso: "pt-PT", file: "pt-pt.json", name: "Portugal" }
    ],
    strategy: "prefix_except_default",
    defaultLocale: "fr",
    lazy: true,
    langDir: "locales/",
    detectBrowserLanguage: false
  },
  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},
  auth: {
    strategies: {
      local: {
        token: {
          property: "token"
          // required: true,
          // type: 'Bearer'
        },
        user: {
          property: "user"
          // autoFetch: true
        },
        endpoints: {
          login: false,
          logout: false,
          user: false
        }
      },
      facebook: {
        endpoints: {
          userInfo:
            "https://graph.facebook.com/v6.0/me?fields=id,name,picture{url}"
        },
        clientId: process.env.FACEBOOK_APP_ID,
        clientSecret: process.env.FACEBOOK_APP_SECRET,
        scope: ["public_profile", "email"]
      },
      google: {
        clientId: process.env.GOOGLE_CLIENT_ID,
        clientSecret: process.env.GOOGLE_CLIENT_SECRET,
        scope: ["openid", "profile", "email"],
        responseType: "code",
        codeChallengeMethod: "",
        grantType: "authorization_code"
      }
    }
  },
  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: "en"
    }
  },

  // Content module configuration: https://go.nuxtjs.dev/config-content
  content: {},

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
  serverMiddleware: [{ path: "/api", handler: "~/server-middleware/rest.js" }],
  plugins: [{ src: "~/plugins/vuex-persist", ssr: false }],
  router: {
    middleware: ["checker"]
  }
};
