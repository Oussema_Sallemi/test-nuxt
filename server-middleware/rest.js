const bodyParser = require("body-parser");
const express = require("express");
const axios = require("axios");
const app = express();

app.use(
  bodyParser.urlencoded({
    extended: true
  })
);
app.use(bodyParser.json());
app.get("/products", async (req, res) => {
  try {
    await axios
      .get(
        "https://migration.lepape.com/rest/default/V1/products/?searchCriteria[filter_groups][0][filters][0][field]=sku&searchCriteria[filter_groups][0][filters][0][value]=MIC11108731,T93KPNJK3,402259,1102851D004&searchCriteria[filter_groups][0][filters][0][condition_type]=in",
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${process.env.MAGENTO_TOKEN}`
          }
        }
      )
      .then(response => {
        res.status(200).send(response.data);
      })
      .catch(err => res.send(err));
  } catch (err) {
    console.error("error from /api/products : ", err);
  }
});

app.post("/auth/login", async (req, res) => {
  try {
    await axios
      .post(
        "https://migration.lepape.com/rest/default/V1/integration/customer/token",
        {
          username: req.body.username,
          password: req.body.password
        }
      )
      .then(response => {
        res.status(200).send(response.data);
      })
      .catch(err => res.send(err));
  } catch (err) {
    console.error("error from /api/auth/login : ", err);
  }
});
app.post("/auth/register", async (req, res) => {
  try {
    await axios
      .post("https://migration.lepape.com/rest/default/V1/customers", {
        customer: {
          email: req.body.email,
          firstname: req.body.firstname,
          lastname: req.body.lastname,
          gender: req.body.gender
        },
        password: req.body.password
      })
      .then(response => {
        res.status(200).send(response.data);
      })
      .catch(err => res.send(err));
  } catch (err) {
    console.error("error from /api/auth/login : ", err);
  }
});

app.post("/auth/users/me", async (req, res) => {
  try {
    let api_response;
    await axios
      .get("https://migration.lepape.com/rest/default/V1/customers/me", {
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${req.body.data}`
        }
      })
      .then(response => {
        api_response = response;
      })
      .catch(err => res.send(err));
    res.status(200).send(api_response.data);
  } catch (err) {
    console.error("error from /api/auth/users/me : ", err);
  }
});

module.exports = app;
